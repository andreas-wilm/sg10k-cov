#1/bin/bash

ST=./samtools-M
IN=./test.sam

test -e $ST || exit 1
test -e $IN || exit 1

res=$($ST view -c -M 0 $IN)
test $res -eq 3 || exit 1
res=$($ST view -c -M 2 $IN)
test $res -eq 2 || exit 1
res=$($ST view -c -M 10 $IN)
test $res -eq 0 || exit 1

echo "success"
