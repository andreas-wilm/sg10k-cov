As agreed on 8th June 2017

1. Before mapping: remove reads with >50% bases <=Q10
2. After mapping: remove duplicates
3. Take all non-clipped bases with at least Q5 into account for coverage

This repo builds a patched samtools version with added -M parameter for samtools view that implements #1
