#!/bin/bash

set -eou pipefail

MOD_ST=samtools-M
WRAPPER=sg10k-cov-062017.sh

version=1.4.1
if [ ! -e samtools-${version}.tar.bz2 ]; then
    echo "Downloading samtools"
    wget -nd https://github.com/samtools/samtools/releases/download/${version}/samtools-${version}.tar.bz2
fi
tar xfj samtools-${version}.tar.bz2
cd samtools-${version}

echo "Building htslib"
cd htslib-*
./configure -disable-libcurl --disable-lzma >/dev/null
make -j 2 >/dev/null
cd ..

echo "Building patched samtools"
patch < ../sam_view.diff
./configure --disable-libcurl --without-curses --disable-lzma >/dev/null
make -j 2 >/dev/null
cp samtools ../${MOD_ST}
cd ..

echo "Testing"
./test.sh

echo "Cleaning up"
rm -rf samtools-${version}.tar.bz2 samtools-${version}

echo "Now: cp ${WRAPPER} ${MOD_ST} to e.g. /usr/local/bin/"

