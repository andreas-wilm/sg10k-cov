#!/bin/bash

set -eou pipefail

# 1. Before mapping: remove reads with >50% bases <=Q10
# 2. After mapping: remove duplicates
# 3. Take all non-clipped bases with at least Q5 into account for coverage


MYDIR=$(dirname $(readlink -f $0))
ST=$MYDIR/samtools-M

if [ "$#" -ne 1 ]; then
    echo "FATAL: need BAM file as only argument" 1>&2
    exit 1
fi
bam=$1
test -z "$bam" && exit 1
test -e "$bam" || exit 1

$ST view -h -F 0x400 -F 0x4 -M 10 $bam | samtools depth -q 5 - | datamash sum 3

